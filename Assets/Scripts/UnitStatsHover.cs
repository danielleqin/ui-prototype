﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitStatsHover : MonoBehaviour {
	public Text health;
	//public Text attack;
	//public Text defence;
	//public Text accuracy;
	
	void Start () {
		health.enabled = false;
		//attack.enabled = false;
		//defence.enabled = false;
		//accuracy.enabled = false;
	}
	
	public void mouseOverStats() {
		Debug.Log("Mouse Over");
		health.enabled = true;
		health.GetComponent<Text>().text = "Health";
		//attack.enabled = true;
		//attack.GetComponent<Text>().text = "Attack";
		//defence.enabled = true;
		//defence.GetComponent<Text>().text = "Defence";
		//accuracy.enabled = true;
		//accuracy.GetComponent<Text>().text = "Accuracy";		
	}
}
